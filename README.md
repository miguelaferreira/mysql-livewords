# msql-livewords

This repository contains the code to build a mysql database docker container ready to be used for development.

The container is build on top of the (official mysql database container)[https://hub.docker.com/_/mysql/].

## DB settings

LiveWords builds require certain settings on MySQL server. Those settings are defined in `livewords.cnf` that gets copied to the container and enabled by default.
